# 测试变量对于运行速度的影响

### 1. CONFIG文件：backpack_2d.lua

#### 测试Demo包：b0-2014-08-14-13-23-01.bag

#### 测试环境： 

***CPU使用情况*** 

```bash
Architecture:        x86_64
CPU(s):              8
On-line CPU(s) list: 0-7
Thread(s) per core:  2
Core(s) per socket:  4
Socket(s):           1
NUMA node(s):        1
Vendor ID:           AuthenticAMD
CPU family:          23
Model:               24
Model name:          AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx
Stepping:            1
CPU MHz:             1469.188
CPU max MHz:         2100.0000
CPU min MHz:         1400.0000
BogoMIPS:            4192.19
Virtualization:      AMD-V
L1d cache:           32K
L1i cache:           64K
L2 cache:            512K
L3 cache:            4096K
NUMA node0 CPU(s):   0-7
```

***空闲状态下CPU以及内存值***

![idel](/src/images-time/idle.png)

##### 变量名字：TRAJECTORY_BUILDER_2D.num_accumulated_range_data

***TRAJECTORY_BUILDER_2D.num_accumulated_range_data = 10***

*`time`测试结果*

```bash
real	12m58.899s
user	19m33.574s
sys		0m51.027s
```

*`top`测试结果*

![10](/src/images-time/backpack-2d-10.png)

内存最高（开始时）3670256 KB

内存最低（快结束时）3079964 KB

***TRAJECTORY_BUILDER_2D.num_accumulated_range_data = 100***

*`time`测试结果*

```bash
real	13m29.749s
user	5m58.477s
sys		0m46.555s
```

*`top`测试结果*

![100](/src/images-time/backpack-2d-100.png)

内存最高（开始时）3665124 KB

内存最低（快结束时）3141396 KB
