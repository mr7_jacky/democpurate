## Demo包Cartographer函数使用的CPU占用率统计（从高到底）

### 下载地址
[Demo下载使用地址](https://google-cartographer-ros.readthedocs.io/en/latest/demos.html#taurob-tracker)

### 运行环境： 

* 系统：Ubuntu 18.04

* CPU：AMD 3500U

* 显卡：AMD Radeon Vega8

* 内存：16GB

### 详细内存信息

####  Deutsches Museum

![museum](./src/images/museum.png)

**红色内存占用**
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates 
```

**绿色内存占用**
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::BranchAndBound  
cartographer::sensor::TransformPointCloud        
cartographer::mapping::PoseGraph2D::constraints
cartographer::mapping::scan_matching::DiscretizeScans
cartographer::mapping::ProbabilityGrid::ApplyLookupTable
cartographer::mapping::PoseGraph2D::GetTrajectoryNodePoses
cartographer::mapping::RayToPixelMask
```
#### Pure Localization

##### mapping

![map](./src/images/pl_map.png)

**红色内存占用**
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
**绿色内存占用**
```bash
cartographer::mapping::ProbabilityGrid::ApplyLookupTable
cartographer::sensor::TransformPointCloud
cartographer::mapping::scan_matching::DiscretizeScans
cartographer::mapping::RayToPixelMask             
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::BranchAndBoun
cartographer::mapping::ProbabilityGrid::DrawToSubmapTexture 
cartographer::mapping::PoseGraph2D::constraints  
```
##### Localization

![loc](./src/images/pl_loc.png)

**红色内存占用**
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
**绿色内存占用**
```bash
cartographer::sensor::TransformPointCloud
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::BranchAndBound
cartographer::mapping::scan_matching::DiscretizeScans
cartographer::mapping::ProbabilityGrid::ApplyLookupTable                    
cartographer::mapping::scan_matching::PrecomputationGrid2D::PrecomputationGrid2D
cartographer::mapping::RayToPixelMask   
```
#### PR2

![pr2](./src/images/pr2.png)

**红色内存占用**
```bash
cartographer::mapping::ProbabilityGrid::GetProbability       
cartographer::mapping::scan_matching::RealTimeCorrelativeScanMatcher2D::ScoreCandidates 
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
**绿色内存占用**
```bash
cartographer::sensor::TransformPointCloud
cartographer::mapping::scan_matching::DiscretizeScans
```
#### Revo LDS

![revo](./src/images/revo.png)

**红色内存占用**
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
**绿色内存占用**
```bash
cartographer::mapping::ProbabilityGrid::GetProbability
```
#### Taurob Tracker
这个包CPU占用率不高

![map](./src/images/tt.png)

### 总结

从上述内存占用情况来看，CPU占用率较多的如下：

**红色内存占用**
```bash
1. cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates 

2. cartographer::mapping::ProbabilityGrid::GetProbability 

3. cartographer::mapping::scan_matching::RealTimeCorrelativeScanMatcher2D::ScoreCandidates 
```
**绿色内存占用**
```bash
4. cartographer::sensor::TransformPointCloud

5. cartographer::mapping::scan_matching::DiscretizeScans

6. cartographer::mapping::ProbabilityGrid::ApplyLookupTable

7. cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::BranchAndBoun

8. cartographer::mapping::RayToPixelMask
```
以下是在个别Demo中，出现占用率较高（绿色）的方法名称：
```bash
1. cartographer::mapping::ProbabilityGrid::DrawToSubmapTexture 
2. cartographer::mapping::PoseGraph2D::constraints  
3. cartographer::mapping::scan_matching::PrecomputationGrid2D::PrecomputationGrid2D
4. cartographer::mapping::ProbabilityGrid::GetProbability
```
