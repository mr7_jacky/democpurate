# [本机测试]关闭后端优化的测试结果

## 测试环境

#### 测试Demo包信息：

##### Demo包名：cartographer_paper_deutsches_museum.bag

##### 使用命令行：roslaunch cartographer_ros demo_backpack_2d.launch bag_filename:=${HOME}/Downloads/cartographer_paper_deutsches_museum.bag

##### `.launch`文件名：demo_backpack_2d.launch

##### `.lua`文件名：backpack_2d.lua 

##### 修改内容：

关闭后端优化，在倒数第二行加入以下内容

```lua
POSE_GRAPH.optimize_every_n_nodes = 0
POSE_GRAPH.constraint_builder.sampling_ratio = 0
POSE_GRAPH.global_sampling_ratio = 0
```

#### CPU情况

```bash
Architecture:        x86_64
CPU(s):              8
On-line CPU(s) list: 0-7
Thread(s) per core:  2
Core(s) per socket:  4
Socket(s):           1
NUMA node(s):        1
Vendor ID:           AuthenticAMD
CPU family:          23
Model:               24
Model name:          AMD Ryzen 5 3500U with Radeon Vega Mobile Gfx
Stepping:            1
CPU MHz:             1469.188
CPU max MHz:         2100.0000
CPU min MHz:         1400.0000
BogoMIPS:            4192.19
Virtualization:      AMD-V
L1d cache:           32K
L1i cache:           64K
L2 cache:            512K
L3 cache:            4096K
NUMA node0 CPU(s):   0-7
```

#### 空闲状态下 *(`top`查询结果)*

![idle](/src/images-opt/idle.png)

#### `idle`值
在空闲状态下，CPU空闲占比均接近100%，其中最低为95.7%，最高为100%。

#### 剩余空闲内存

在`KiB Mem`这一栏我们可以看到以下数据*（单位：KiB）*：

|total|free|used|buff/cache|
|--|--|--|--|
|15363568|9908508|1908320|3546740|

## 未关闭后端优化测试数据

#### `time`查询测试时间

```bash
real	33m1.156s
user	105m18.317s
sys     1m46.013s
```
#### `rviz`帧率：

|最高|最低|众数|
|-|-|-|
|31fps|9fps|20fps|

#### `top`查询CPU状态

![cpu](/src/images-opt/cpu.png)

1. CPU分配不均，最大空闲率和最小空闲率的CPU差值在50%以上。
2. CPU分配波动较快较大。有时会出现空闲率90%以上和5%左右的情况

#### `top`查询运行末期剩余内存状态

![mem](/src/images-opt/mem.png)

在`KiB Mem`这一栏我们可以看到以下数据*（单位：KiB）*：

|total|free|used|buff/cache|
|--|--|--|--|
|15363568|7330444|4531924|35011200|

总共使用内存：2578064KiB

#### `perf record` 数据

![PERF](/src/images-opt/perf.png)

CPU占用率最高的函数为：
|对象名|函数名|占用比|
|-|-|-|
|`cartographer_node`|`cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates`|62.16%|

## 关闭后端优化后测试数据

#### `time`查询测试时间

```bash
real	46m40.051s
user	13m39.566s
sys     2m26.734s
```
#### `rviz`帧率

|最高|最低|众数|
|-|-|-|
| 31fps |  31fps| 31fps|

#### `top`查询CPU状态

![cpu](/src/images-opt/opt-cpu.png)

1. CPU分配几乎很平均，最大空闲率和最小空闲率的CPU差值在20%以内。
2. CPU分配波动较快较小。

#### `top`查询运行末期剩余内存状态

![mem](/src/images-opt/opt-mem.png)

在`KiB Mem`这一栏我们可以看到以下数据*（单位：KiB）*：

|total|free|used|buff/cache|
|--|--|--|--|
|15363568|8971764|2619268|3772536|

总共使用内存：936744KiB

与未关闭后端优化时，内存占用减少：1641320KiB

#### `perf record` 数据

![perf](/src/images-opt/perf-opt.png)

CPU占用率最高的函数为：

|对象名|函数名|占用比|
|-|-|-|
|`cartographer_node`|`cartographer::mapping::RayToPixelMask`|10.08%|
|`cartographer_node`|`cartographer::mapping::ProbabilityGrid::ApplyLookUpTable`|9.92%|
|`libbz2.so.1.0.4`|`BZ2_decompress`|9.49%|
|`libbz2.so.1.0.4`|`BZ2_bzDecompress`|8.42%|
