# VirtualBox Ubuntu 18.04 环境下运行 Ros Melodic + Cartographer

## 1. 运行参数

##### 1.1 CPU信息:

*通过 `lscpu` 查询*

```bash
Architecture: x86_64 
CPU op-mode(s): 32-bit, 64-bit 
CPU(s): 2 
On-line CPU(s) list: 0,1 
Thread(s) per core: 1 
Core(s) per socket: 2 Socket(s): 1 
NUMA node(s): 1 
CPU family: 6 
Model name: Intel(R) Core(TM) i5-8210Y CPU @ 1.60GHz 
Stepping: 9 
CPU MHz: 1607.999 
BogoMIPS: 3215.99 
Virtualization type: full 
L1d cache: 32K 
L1i cache: 32K 
L2 cache: 256K 
L3 cache: 4096K 
```

##### 1.2 内存信息(MB)：

*通过`free -m`查询*

```bash
total: 5527
```

##### 1.3 空闲状态下

![top](/src/images/idle.png)

| 剩余内存 | CPU0 idle | CPU1 idle|
| ------ | ---------| ----- |
| 3903800 KB | 85.9 | 87.4 |

## 2. 测试结果

### 2.1 Museum（无`rviz`）

#### 2.1.1 Demo信息

通过以下命令行下载并允许软件包

```bash
wget -P ~/Downloads https://storage.googleapis.com/cartographer-public-data/bags/backpack_2d/cartographer_paper_deutsches_museum.bag
roslaunch cartographer_ros demo_backpack_2d.launch bag_filename:=${HOME}/Downloads/cartographer_paper_deutsches_museum.bag
```

##### 1. 软件包：cartographer_paper_deutsches_museum.bag

##### 2. Demo包大小：470MB

##### 3. 地图大小 3066平方米


#### 2.1.2 运行结果

***使用 `top` 查询CPU使用情况以及内存使用情况，如图***

![top](/src/images/museum-top.png)

![top](/src/images/museum-top-end.png)

我们可以看出两个CPU的使用分配不是很均匀，大部分时间都会有大约10-20%左右的CPU占用率差值。其中`id`表示idle，即空闲CPU占用比。刚刚开始时CPU空闲比例会剩余25%左右（上图），但在运行5-6分钟后（下图），空闲CPU的占比是0%，说明无空闲CPU。由此我们可以推出当前运行环境，不能完全有余力的使用Cartographer。

我们再来看内存。空闲状态下free内存有3903800KB的内存，运行5-6分钟后时剩余3345236K，约使用内存558564KB。说明内存足够正常运行这个包。

***使用 `perf record` 记录的结果，如下图***

参考[系统级性能分析工具perf的介绍与使用](https://www.cnblogs.com/arnoldlu/p/6241297.html)

![museum](/src/images/museum-perf.png)

我们可以看到函数
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
该函数CPU占用率占该命令的49.17，是占内存最多的一个函数。其次是一个`libbz2.so.1.04`library的函数，该函数占用率占比达到5.21%。其余函数的占比均在3%以下，并没有达到红色的占用率，因此对CPU影响较小。


### 2.2 Pure localization（无`rviz`）

#### 2.2.1 Demo信息

通过以下命令行下载软件包

```bash
wget -P ~/Downloads https://storage.googleapis.com/cartographer-public-data/bags/backpack_2d/b2-2016-04-05-14-44-52.bag
wget -P ~/Downloads https://storage.googleapis.com/cartographer-public-data/bags/backpack_2d/b2-2016-04-27-12-31-41.bag
```

*Demo包信息*
| Demo包名称 | 预计运行时间 | 大小 |
| -------- | ---------- | ---- |
| b2-2016-04-05-14-44-52.bag | 360 s | 94 MB |	
| b2-2016-04-27-12-31-41.bag | 881 s | 234 MB |

*地图大小*

3284平方米


#### 2.2.2 运行结果

***使用 `time` 计算运行时间如下***

建模用时：

```bash
real	9m44.212s
user    15m25.363s
sys     1m56.097s
```

***使用 `top` 查询CPU使用情况以及内存使用情况，如图***

在`b2-2016-04-05-14-44-52.bag`包中：

![top](/src/images/pure-map-top.png)

我们可以看到`idle`值剩余34.7%和41.5%说明对于这个包完全能够运行。但仍然占用了大部分的CPU。对于内存剩余3447828KB，该包使用了大约455972KB的内存，使用内存并不是很多。

在`b2-2016-04-27-12-31-41.bag`包中：

![top](/src/images/pure-top-loc.png)

在定位的时候，CPU的分配较为均匀并没有出现很大的波动。但`id`值始终为0。因此CPU剩余百分比不足。

***使用 `perf record` 记录的结果，如下图***

![museum](/src/images/pure-perf-map.png)

我们可以看到函数
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
该函数CPU占用率，占该命令的44.60%，是占内存最多的一个函数。其次是一个`libbz2.so.1.04`library的函数，该函数占用率占比达到6.17%。其余函数的占比均在5%以下，并没有达到红色的占用率，因此对CPU影响较小。

![museum](/src/images/pure-perf-loc.png)

我们可以看到函数
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
该函数CPU占用率较高，占该命令的42.91%，是占内存最多的一个函数。虽然该函数相比建图时的使用率低，但仍然处于较高的使用率。其次是`libbz2.so.1.0.4`的`BZ2_bzDecompress`占用了6.02%。其余函数的占比均在5%以下，并没有达到红色的占用率，因此对CPU影响较小。


### 2.3 Revo LDS

#### 2.3.1 Demo信息

使用命令行下载并运行

```bash
wget -P ~/Downloads https://storage.googleapis.com/cartographer-public-data/bags/revo_lds/cartographer_paper_revo_lds.bag
roslaunch cartographer_ros demo_revo_lds.launch bag_filename:=${HOME}/Downloads/cartographer_paper_revo_lds.bag
```

##### 1. 软件包：cartographer_paper_revo_lds.bag

##### 2. 大小：3.03MB

##### 3. 地图大小：519平方米

#### 2.2.2 运行结果

***使用 `top` 查询CPU使用情况以及内存使用情况，如图***


![top](/src/images/devo-top.png)
![top](/src/images/devo-top-rviz.png)

这两张图分别是在有`rviz`和没有`rviz`的情况下的检测。可以看到对于较小的bag来说，`rviz`占用的内存相对`cartographer`来说较大。如果关闭`rviz`等GUI功能可以极大的提高运行所占CPU。可以看到在非`rviz`模式下（上图）`id`剩余CPU占比达到了73.4%和72.2%，相对比之下打开`rviz`之后（下图），剩余CPU仅剩42.0%和45.6%。

然而无论是否开启`rviz`，内存使用情况都相近，未开启`rviz`时，剩余内存为1704280KB，开启`rviz`时，内存剩余则为1628096KB。

***在未使用`rviz`时，使用 `perf record` 记录的结果，如下图***

![museum](/src/images/devo-perf.png)

我们可以看到函数
```bash
cartographer::mapping::scan_matching::FastCorrelativeScanMatcher2D::ScoreCandidates
```
该函数CPU占用率较高，占该命令的42.91%，是占内存最多的一个函数。其余函数的占比均在5%以下，并没有达到红色的占用率，因此对CPU影响较小。
